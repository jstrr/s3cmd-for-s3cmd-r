#!/bin/bash

# ------------------------------------------------------------------------------------------ 
# Asterisk Backup with S3
# note: to push the files to Amazon S3 you need to install s3cmd from http://s3tools.org/s3cmd
# author: Alexander Ababii (alexcr@pbxware.ru)
# -

BUCKED="stavka.ru"
ASTERNAME="stavka.ru"  									# name of server and bucket on S3Amazon
TODAYS_BACKUP_DIRECTORY=$ASTERNAME"_backup_`date +%d%m%Y`"				# name of the backup folder, format: ASTERNAME_backup_ddmmyyyy 
BACKUP_DIRECTORY="/backup/"								# location on disk where backup is written
BACKUP_DESTINATION=$BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY"/source_`date +%s`.tar"	# full path to source will be tarred 

# report specific 
REPORT_LOCATION=$BACKUP_DIRECTORY"/report_`date +%s`.txt"	# detailed report of the backup
REPORT_NEWLINE="-----------------------------------------------------"			# new line in report
EMAIL_RECIPIENT="info@pbxware.ru"							# email recipient of the report
EMAIL_SUBJECT=$ASTERNAME" Backup for `date +%m%d%Y`"		

# MySQL specific 
MYSQL_USERNAME="freepbxuser"								# mysql username
MYSQL_PASSWORD="LAx0yGR0u8Ga"								# mysql password
MYSQLDUMP_FILENAME="/db_`date +%s`.sql.gz"						# database dump name
MYSQLDUMP_LOCATION=$BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY$MYSQLDUMP_FILENAME		# full path to database dump
#DATABASE_BACKUP_LIST="all"							# database(s) to backup

# ETC folder location 
ETC_LOCATION="/etc/"
# /usr/lib/asterisk/modules
MODULES_LOCATION="/usr/lib/asterisk/modules/"
# /var/lib/asterisk
LIB_LOCATION="/var/lib/asterisk/"
# /var/spool/asterisk
SPOOL_LOCATION="/var/spool/asterisk"
# /var/www/
WEB_location="/var/www/"
